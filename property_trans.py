#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw 
# @generate at 2024/1/17 14:52

import mysql.connector
from datetime import datetime
import configparser
from loguru import logger
import os
import requests

# 配置、日志设置
config = configparser.RawConfigParser()
config.read("db.conf")
logDir = os.path.expanduser("/root/code/zrprod/")
if not os.path.exists(logDir):
    os.mkdir(logDir)
logFile = os.path.join(logDir, "property.log")
logger.remove(handler_id=None)

logger.add(
    logFile,
    rotation="1 year",
    retention="3 years",
    format="{time:YYYY-MM-DD at HH:mm:ss} | {level} | {message}",  # 自定义日志格式
    level="INFO"
)

# 配置信息读取
src_host = config.get("prod", "host")
src_port = int(config.get("prod", "port"))
src_database = config.get("prod", "database")
src_user = config.get("prod", "user")
src_password = config.get("prod", "password")

tar_host = config.get("fin_main", "host")
tar_database = config.get("fin_main", "database")
tar_user = config.get("fin_main", "user")
tar_password = config.get("fin_main", "password")

wx_key = config.get("wx_test", "r_key")
wx_url = "https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key={}"
wx_headers = {"Content-Type": "application/json"}
wx_mentioned_list = [""]

# SQL

sql_read_1 = "SELECT m.Id AS MaterialId, m.MaterialName AS MaterialName, p.Id AS BusinessBelongId, p.PropName AS BusinessBelongName,NOW() AS InsertTime FROM tb_materialproperties AS mp JOIN tb_materialinfo m ON mp.MaterialId = m.Id AND m.Deleted = 0 JOIN tb_properties p ON mp.PropId = p.Id WHERE mp.Deleted = 0 AND mp.PropParentId = 'PR0000001466';"
sql_tru_1 = "TRUNCATE TABLE finance_main.config_materialbusinessbelong"
sql_write_1 = "INSERT INTO finance_main.config_materialbusinessbelong(MaterialId,MaterialName,BusinessBelongId,BusinessBelongName,InsertTime) VALUES(%s, %s, %s, %s, %s)"


def send_msg(content_msg, in_wx_key):
    data = {
        "msgtype": "text",
        "text": {"content": content_msg, "mentioned_list": wx_mentioned_list},
    }
    r = requests.post(url=wx_url.format(in_wx_key), json=data, headers=wx_headers)
    return r


logger.info('配置数据准备完毕!')

# 开始时间
start_time = datetime.now()
year = start_time.year
month = start_time.month
cur_mon = f"{year:04d}年-{month:02d}月"

try:

    # 连接建立
    src_con = mysql.connector.connect(
        host=src_host,
        port=src_port,
        user=src_user,
        password=src_password,
        database=src_database,
        buffered=True,
    )

    tar_con = mysql.connector.connect(
        host=tar_host,
        user=tar_user,
        password=tar_password,
        database=tar_database,
        buffered=True,
    )

    logger.info('连接创建成功！')

    # 读取数据
    src_cur = src_con.cursor()

    src_cur.execute(sql_read_1)
    src_res_1 = src_cur.fetchall()

    src_cur.close()

    logger.info('数据读取完毕!')

    # 处理数据

    # 写入数据
    tar_cur = tar_con.cursor()

    tar_cur.execute(sql_tru_1)

    logger.info('目标库表清理完毕!')

    tar_cur.executemany(sql_write_1, src_res_1)
    af_rows_1 = tar_cur.rowcount

    logger.info('目标库表写入成功!')

    tar_cur.close()
    tar_con.commit()

    # 发送企业微信消息
    end_time = datetime.now()
    sp_second = (end_time - start_time).total_seconds()
    logger.info(f"全部数据迁移完成，累计耗时 {sp_second} 秒！")

    wx_message = f'{cur_mon}产品中心物料业务所属数据同步完毕，共{af_rows_1}条，耗时{sp_second}秒。'
    send_msg(wx_message, wx_key)
    # logger.info(wx_message)

    logger.info('微信消息发送成功!')

except Exception as e:
    tar_con.rollback()
    logger.exception('数据迁移失败！原因：', e)

finally:
    src_con.close()
    tar_con.close()
